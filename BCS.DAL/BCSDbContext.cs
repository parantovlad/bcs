﻿using BCS.DAL.Repository;
using BCS.DAL.UnitOfWork;
using BCS.Entity;
using Microsoft.EntityFrameworkCore;

namespace BCS.DAL
{
    public class BCSDbContext : EFUnitOfWork, IBCSUnitOfWork
    {
        public BCSDbContext(DbContextOptions<BCSDbContext> options) : base(options)
        {
            OperationRepository = new ExtendedRepository<Operation>(this, Operations);
        }

        public IExtendedRepository<Operation> OperationRepository { get; private set; }

        public virtual DbSet<Operation> Operations { get; set; }
    }
}
