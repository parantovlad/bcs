﻿using System;

namespace BCS.DAL.UnitOfWork
{
    public class UnitOfWorkSaveChangesException : Exception
    {
        public UnitOfWorkSaveChangesException()
        {

        }

        public UnitOfWorkSaveChangesException(string message)
            : base(message)
        {

        }

        public UnitOfWorkSaveChangesException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
