﻿using System;

namespace BCS.DAL.UnitOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        void SaveChanges();
    }
}
