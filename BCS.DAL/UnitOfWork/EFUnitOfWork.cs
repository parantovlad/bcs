﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BCS.DAL.UnitOfWork
{
    public class EFUnitOfWork : DbContext, IUnitOfWork, IDisposable
    {
        public EFUnitOfWork()
        {

        }

        public EFUnitOfWork(DbContextOptions options)
            : base(options)
        {

        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                ex.Entries.Single().Reload();
                throw new UnitOfWorkSaveChangesException("DbUpdateConcurrencyException", ex);
            }
        }

        void IUnitOfWork.SaveChanges()
        {
            this.SaveChanges();
        }
    }
}
