﻿using BCS.DAL.Repository;
using BCS.Entity;

namespace BCS.DAL.UnitOfWork
{
    public interface IBCSUnitOfWork : IUnitOfWork
    {
        IExtendedRepository<Operation> OperationRepository { get; }
    }
}
