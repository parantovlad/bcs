﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BCS.DAL.Repository
{
    public interface IExtendedRepository<TEntity>: IRepository<TEntity> where TEntity: class
    {
        IQueryable<TEntity> Include(IEnumerable<Expression<Func<TEntity, object>>> paths);
    }
}
