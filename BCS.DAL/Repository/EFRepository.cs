﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace BCS.DAL.Repository
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        private readonly DbSet<TEntity> _dbSet;

        public EFRepository(DbSet<TEntity> dbSet)
        {
            this._dbSet = dbSet;
        }

        public Type ElementType
        {
            get
            {
                return ((IQueryable)this._dbSet).ElementType;
            }
        }

        public Expression Expression
        {
            get
            {
                return ((IQueryable)this._dbSet).Expression;
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                return ((IQueryable)this._dbSet).Provider;
            }
        }

        public TEntity Add(TEntity entity)
        {
            return this._dbSet.Add(entity).Entity;
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            this._dbSet.AddRange(entities);
        }

        public TEntity Attach(TEntity entity)
        {
            return this._dbSet.Attach(entity).Entity;
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity: class, TEntity, new()
        {
            return (TDerivedEntity)this._dbSet.Add(new TDerivedEntity()).Entity;
        }

        public async Task<TEntity> Create()
        {
            return (await this._dbSet.AddAsync(new TEntity())).Entity;
        }

        public TEntity Find(params object[] keyValues)
        {
            return this._dbSet.Find(keyValues);
        }

        public ValueTask<TEntity> FindAsync(params object[] keyValues)
        {
            return this._dbSet.FindAsync(keyValues);
        }

        public ValueTask<TEntity> FindAsync(CancellationToken cancellationToken, params object[] keyValues)
        {
            return this._dbSet.FindAsync(cancellationToken, keyValues);
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return ((IEnumerable<TEntity>)this._dbSet).GetEnumerator();
        }

        public TEntity Remove(TEntity entity)
        {
            return this._dbSet.Remove(entity).Entity;
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            this._dbSet.RemoveRange(entities);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this._dbSet).GetEnumerator();
        }
    }
}
