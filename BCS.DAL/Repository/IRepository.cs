﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BCS.DAL.Repository
{
    public interface IRepository<TEntity>: IQueryable<TEntity> where TEntity: class
    {
        TEntity Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        TEntity Attach(TEntity entity);
        TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, TEntity, new();
        Task<TEntity> Create();
        TEntity Find(params object[] keyValues);
        ValueTask<TEntity> FindAsync(params object[] keyValues);
        ValueTask<TEntity> FindAsync(CancellationToken cancellationToken, params object[] keyValues);
        TEntity Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}
