﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BCS.DAL.Repository
{
    public class ExtendedRepository<TEntity>: EFRepository<TEntity>, IExtendedRepository<TEntity> where TEntity: class, new()
    {
        public DbContext Context { get; private set; }
        private readonly DbSet<TEntity> _dbSet;

        public ExtendedRepository(DbContext context, DbSet<TEntity> dbSet) : base(dbSet)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = dbSet;
        }

        public IQueryable<TEntity> Include(IEnumerable<Expression<Func<TEntity, object>>> paths)
        {
            var query = _dbSet.AsQueryable();
            foreach (var path in paths)
            {
                query = query.Include(path);
            }

            return query;
        }
    }

    public static class IncludeExtensions
    {
        public static IQueryable<TEntity> Include<TEntity, TProperty>(this IQueryable<TEntity> entities,
            Expression<Func<TEntity, TProperty>> paths) where TEntity : class
        {
            return EntityFrameworkQueryableExtensions.Include(entities, paths);
        }
    }
}
