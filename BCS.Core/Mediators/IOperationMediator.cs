﻿using BCS.Entity;

namespace BCS.Core.Mediators
{
    public interface IOperationMediator
    {
        void CreateOperation(Operation operation);
    }
}
