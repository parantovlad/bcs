﻿using BCS.Entity;

namespace BCS.Core.Messaging.Sender
{
    public interface IOperationCreateSender
    {
        void CreateOperation(Operation operation);
    }
}
