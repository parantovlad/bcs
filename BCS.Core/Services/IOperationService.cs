﻿using BCS.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BCS.Core.Services
{
    public interface IOperationService
    {
        IEnumerable<Operation> GetAll();
        Operation GetById(Guid id);
        Task<Operation> Create(Operation operation);
        void Update(Guid id, Operation operation);
        void Delete(Guid id);
    }
}
