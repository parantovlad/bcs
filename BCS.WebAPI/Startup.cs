using BCS.BLL.Commands;
using BCS.BLL.Mediators;
using BCS.BLL.Messaging.Options;
using BCS.BLL.Messaging.Receivers;
using BCS.BLL.Services;
using BCS.Core.Mediators;
using BCS.Core.Services;
using BCS.DAL;
using BCS.DAL.UnitOfWork;
using BCS.Entity;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Reflection;

namespace BCS.WebAPI
{
    public class Startup
    {
        private const string ConnectionName = "BCS";

        public Startup(IConfiguration configuration, IHostEnvironment environment)
        {
            Environment = environment;
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.EnvironmentName}.json")
                .Build();
        }

        public IConfiguration Configuration { get; }
        public IHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            var serviceClientSettingsConfig = Configuration.GetSection("RabbitMq");
            var serviceClientSettings = serviceClientSettingsConfig.Get<RabbitMqConfiguration>();
            services.Configure<RabbitMqConfiguration>(serviceClientSettingsConfig);

            AddDbContext(services);
            AddServices(services);

            if (serviceClientSettings.Enabled)
            {
                services.AddHostedService<OperationCreateReceiver>();
            }

            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = Configuration.GetConnectionString("Redis");
            });

            services.AddControllers();
            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, BCSDbContext dataContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            dataContext.Database.Migrate();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BCS API V1");
                c.RoutePrefix = string.Empty;
            });

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void AddDbContext(IServiceCollection services)
        {
            var connectionString = System.Environment.GetEnvironmentVariable(ConnectionName) ?? Configuration.GetConnectionString(ConnectionName);
            services.AddDbContext<BCSDbContext>(options => options.UseNpgsql(connectionString));
        }

        private void AddServices(IServiceCollection services)
        {
            services.AddScoped(typeof(IBCSUnitOfWork), typeof(BCSDbContext));

            services.AddMediatR(Assembly.GetExecutingAssembly(), typeof(IOperationMediator).Assembly);
            services.AddTransient<IOperationMediator, OperationMediator>();

            services.AddTransient<IOperationService, OperationService>();

            services.AddTransient<IRequestHandler<CreateOperationCommand, Operation>, CreateOperationCommandHandler>();
        }
    }
}
