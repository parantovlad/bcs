﻿using BCS.Core.Services;
using BCS.Entity;
using BCS.WebAPI.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperationController : ControllerBase
    {
        private readonly IDistributedCache _cache;
        private readonly IOperationService _operationService;

        public OperationController(IDistributedCache cache, IOperationService operationService)
        {
            _cache = cache;
            _operationService = operationService;
        }

        // GET: api/<OperationController>
        [HttpGet]
        public async Task<IEnumerable<Operation>> Get()
        {
            var cacheKey = "OperationList";
            var operations = await _cache.GetCacheValueAsync<IEnumerable<Operation>>(cacheKey);
            if (operations != null)
            {
                return operations;
            }
            else
            {
                var result = _operationService.GetAll();
                await _cache.SetCacheValueAsync(cacheKey, result?.ToList());
                return result;
            }
        }

        // GET api/<OperationController>/5
        [HttpGet("{id}")]
        public Operation Get(Guid id)
        {
            return _operationService.GetById(id);
        }

        // POST api/<OperationController>
        [HttpPost]
        public void Post([FromBody] Operation operation)
        {
            _operationService.Create(operation);
        }

        // PUT api/<OperationController>/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody] Operation operation)
        {
            _operationService.Update(id, operation);
        }

        // DELETE api/<OperationController>/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _operationService.Delete(id);
        }
    }
}
