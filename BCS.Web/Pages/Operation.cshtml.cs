using System;
using System.Collections.Generic;
using System.Net.Http;
using BCS.Entity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace BCS.Web.Pages
{
    public class OperationModel : PageModel
    {
        private readonly ILogger<OperationModel> _logger;
        private readonly IOptions<Settings> _settings;

        public OperationModel(ILogger<OperationModel> logger
            , IOptions<Settings> settings)
        {
            _logger = logger;
            _settings = settings;
        }

        public Operation Operation { get; set; }

        public void OnGet(string id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage();
                    request.RequestUri = new Uri($"http://{_settings.Value.WebApi}/api/Operation/{id}");
                    var response = client.SendAsync(request).Result;
                    if (response != null)
                    {
                        var jsonString = response.Content.ReadAsStringAsync().Result;
                        Operation = JsonConvert.DeserializeObject<Operation>(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(null, ex, ex.Message);
            }
        }
    }
}
