﻿using BCS.Core.Messaging.Sender;
using BCS.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace BCS.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IOperationCreateSender _operationCreateSender;
        private readonly IOptions<Settings> _settings;

        public IndexModel(ILogger<IndexModel> logger
            , IOperationCreateSender operationCreateSender
            , IOptions<Settings> settings)
        {
            _logger = logger;
            _operationCreateSender = operationCreateSender;
            _settings = settings;
        }

        [BindProperty]
        public Guid ForeignId { get; set; }
        [BindProperty]
        public DateTime OrderDate { get; set; }
        [BindProperty]
        public string SubaccountCode { get; set; }

        public IEnumerable<Operation> Operations { get; set; }

        public void OnGet()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage();
                    request.RequestUri = new Uri($"http://{_settings.Value.WebApi}/api/Operation");
                    var response = client.SendAsync(request).Result;
                    if (response != null)
                    {
                        var jsonString = response.Content.ReadAsStringAsync().Result;
                        Operations = JsonConvert.DeserializeObject<IEnumerable<Operation>>(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(null, ex, ex.Message);
            }
        }

        public IActionResult OnPost()
        {
            _operationCreateSender.CreateOperation(new Operation
            {
                ForeignId = ForeignId,
                OrderDate = OrderDate,
                SubaccountCode = SubaccountCode
            });

            return RedirectToPage("Index");
        }
    }
}
