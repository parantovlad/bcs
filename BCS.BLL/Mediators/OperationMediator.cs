﻿using BCS.BLL.Commands;
using BCS.Core.Mediators;
using BCS.Entity;
using MediatR;
using System;
using System.Diagnostics;

namespace BCS.BLL.Mediators
{
    public class OperationMediator: IOperationMediator
    {
        private readonly IMediator _mediator;

        public OperationMediator(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async void CreateOperation(Operation operation)
        {
            try
            {
                await _mediator.Send(new CreateOperationCommand
                {
                    Operation = operation
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
