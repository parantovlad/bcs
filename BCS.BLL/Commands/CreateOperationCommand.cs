﻿using BCS.Entity;
using MediatR;

namespace BCS.BLL.Commands
{
    public class CreateOperationCommand : IRequest<Operation>
    {
        public Operation Operation { get; set; }
    }
}
