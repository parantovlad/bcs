﻿using BCS.Core.Services;
using BCS.Entity;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using System.Threading.Tasks;

namespace BCS.BLL.Commands
{
    public class CreateOperationCommandHandler : IRequestHandler<CreateOperationCommand, Operation>
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public CreateOperationCommandHandler(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        public async Task<Operation> Handle(CreateOperationCommand request, CancellationToken cancellationToken)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var _operationService = scopedServices.GetRequiredService<IOperationService>();
                return await _operationService.Create(request.Operation);
            }
        }
    }
}
