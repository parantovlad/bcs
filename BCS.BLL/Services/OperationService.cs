﻿using BCS.BLL.Exceptions;
using BCS.Core.Services;
using BCS.DAL.UnitOfWork;
using BCS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCS.BLL.Services
{
    public class OperationService: IOperationService
    {
        private readonly IBCSUnitOfWork _unitOfWork;

        public OperationService(IBCSUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Operation> GetAll()
        {
            return _unitOfWork.OperationRepository.Select(o => o);
        }

        public Operation GetById(Guid id)
        {
            return _unitOfWork.OperationRepository.FirstOrDefault(o => o.Id == id);
        }

        public async Task<Operation> Create(Operation operation)
        {
            var newOperation = await _unitOfWork.OperationRepository.Create();
            newOperation.ForeignId = operation.ForeignId;
            newOperation.OrderDate = operation.OrderDate;
            newOperation.SubaccountCode = operation.SubaccountCode;

            newOperation.CreateDate = DateTime.UtcNow;
            newOperation.UpdateDate = DateTime.UtcNow;

            _unitOfWork.SaveChanges();

            return newOperation;
        }

        public void Update(Guid id, Operation operation)
        {
            var updateOperation = _unitOfWork.OperationRepository.FirstOrDefault(o => o.Id == id);

            if (updateOperation == null)
            {
                throw new NotFoundException("Operation Not Found");
            }
            updateOperation.ForeignId = operation.ForeignId;
            updateOperation.OrderDate = operation.OrderDate;
            updateOperation.SubaccountCode = operation.SubaccountCode;

            updateOperation.UpdateDate = DateTime.UtcNow;

            _unitOfWork.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var deleteOperation = _unitOfWork.OperationRepository.FirstOrDefault(o => o.Id == id);

            if (deleteOperation == null)
            {
                throw new NotFoundException("Operation Not Found");
            }

            _unitOfWork.OperationRepository.Remove(deleteOperation);

            _unitOfWork.SaveChanges();
        }
    }
}
