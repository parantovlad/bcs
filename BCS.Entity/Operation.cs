﻿using System;

namespace BCS.Entity
{
    public class Operation : BaseEntity
    {
        public Guid ForeignId { get; set; }
        public DateTime OrderDate { get; set; }
        public string SubaccountCode { get; set; }
    }
}
